import React from "react"
import { Fabric } from "office-ui-fabric-react"
import "./index.css"

const App: React.FunctionComponent = () => {
  return (
    <Fabric className="App">
    <div className="container">
      <img src="/auzebirds.png"/>
      <span>will be available soon</span>
    </div>
    </Fabric>
  )
}

export default App
